package com.omg.customercenter.model.request;

import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class CustomerSearchRequest {

    private Long customerId;

}
