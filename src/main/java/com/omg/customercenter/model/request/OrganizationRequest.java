package com.omg.customercenter.model.request;

import lombok.Builder;
import lombok.Data;

import javax.validation.constraints.NotBlank;

@Data
@Builder
public class OrganizationRequest {

    @NotBlank
    private String code;

    @NotBlank
    private String name;

    private String address;

    private String phone;

    private String description;

}
