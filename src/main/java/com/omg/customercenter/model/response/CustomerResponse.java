package com.omg.customercenter.model.response;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.omg.customercenter.utils.LocalDateTimeDeserializer;
import com.omg.customercenter.utils.LocalDateTimeSerializer;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
public class CustomerResponse extends BaseResponse {

    private String firstName;

    private String lastName;

    private String address;

    private String phone;

    private String type;

    private String organizationCode;

    private String organizationName;

    private String organizationPhone;

    private boolean isDeleted;

    private String createdBy;

    private String updatedBy;

    @JsonSerialize(using = LocalDateTimeSerializer.class)
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    private LocalDateTime createdTime;

    @JsonSerialize(using = LocalDateTimeSerializer.class)
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    private LocalDateTime updatedTime;

}
