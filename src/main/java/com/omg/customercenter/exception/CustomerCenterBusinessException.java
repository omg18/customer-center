package com.omg.customercenter.exception;

/**
 * @author cuongnh
 * The custom exception to handle the business exception
 */
public class CustomerCenterBusinessException extends Exception{

    public CustomerCenterBusinessException(String errorMessage){
        super(errorMessage);
    }
}
