package com.omg.customercenter.constant;

/**
 * @author HieuDN
 * This class defines all enum of customer.
 */
public enum CustomerEnum {
    INDIVIDUAL("INDIVIDUAL"),
    ORGANIZATION("ORGANIZATION");

    private final String value;

    CustomerEnum(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
