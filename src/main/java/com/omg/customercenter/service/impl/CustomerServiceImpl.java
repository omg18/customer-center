package com.omg.customercenter.service.impl;

import com.omg.customercenter.constant.CustomerEnum;
import com.omg.customercenter.entity.CustomerEntity;
import com.omg.customercenter.entity.OrganizationEntity;
import com.omg.customercenter.exception.CustomerCenterBusinessException;
import com.omg.customercenter.model.request.CustomerRequest;
import com.omg.customercenter.model.request.CustomerSearchRequest;
import com.omg.customercenter.model.response.CustomerResponse;
import com.omg.customercenter.repository.CustomerRepository;
import com.omg.customercenter.repository.OrganizationRepository;
import com.omg.customercenter.service.ICustomerService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Objects;
import java.util.Optional;

/**
 * @author hieudn
 * CategoryServiceImpl
 */
@Service
@Slf4j
public class CustomerServiceImpl implements ICustomerService {

    @Autowired
    private OrganizationRepository organizationRepository;

    @Autowired
    private CustomerRepository customerRepository;

    /**
     * @param request
     * @throws CustomerCenterBusinessException
     */
    @Override
    @Transactional
    public void createCustomer(CustomerRequest request) throws CustomerCenterBusinessException {

        // Check exist the item with the code in the database
        if (request.getType().equals(CustomerEnum.INDIVIDUAL.getValue())) {
            request.setOrganizationId(null);
        } else if (request.getType().equals(CustomerEnum.ORGANIZATION.getValue())) {
            if (Objects.isNull(request.getOrganizationId())) {
                throw new CustomerCenterBusinessException("organizationId cannot be null with customer type equals ORGANIZATION");
            }
        } else {
            throw new CustomerCenterBusinessException("Invalid customer type");
        }

        // check if not exist organizationId
        if (!Objects.isNull(request.getOrganizationId())
                && this.organizationRepository.findById(request.getOrganizationId()).isEmpty()) {
            throw new CustomerCenterBusinessException("cannot find this organizationId: " + request.getOrganizationId());
        }

        // Prepare data for create new item
        CustomerEntity customerEntity = CustomerEntity.builder().firstName(request.getFirstName())
                .lastName(request.getLastName())
                .address(request.getAddress())
                .phone(request.getPhone())
                .type(request.getType())
                .organizationId(request.getOrganizationId())
                .isDeleted(false).build();

        this.customerRepository.save(customerEntity);
    }

    /**
     * get customer info by criteria
     *
     * @param customerSearchRequest
     * @return
     * @throws CustomerCenterBusinessException
     */
    @Override
    public CustomerResponse getCustomerInfo(CustomerSearchRequest customerSearchRequest) throws CustomerCenterBusinessException {
        // Check exist the customer with the id in the database
        Optional<CustomerEntity> customerEntityOptional = customerRepository.findById(customerSearchRequest.getCustomerId());
        if (customerEntityOptional.isEmpty()) {
            throw new CustomerCenterBusinessException("no data for id: " + customerSearchRequest.getCustomerId());
        }

        // create response value
        CustomerEntity customerEntity = customerEntityOptional.get();
        CustomerResponse responseResult = convertCustomerResponse(customerEntity);

        // get organization info
        if (!Objects.isNull(customerEntity.getOrganizationId())) {
            Optional<OrganizationEntity> organizationEntityOptional = organizationRepository.findById(customerEntity.getOrganizationId());

            if (organizationEntityOptional.isPresent()) {
                OrganizationEntity organizationEntity = organizationEntityOptional.get();
                responseResult.setOrganizationCode(organizationEntity.getCode());
                responseResult.setOrganizationName(organizationEntity.getName());
                responseResult.setOrganizationPhone(organizationEntity.getPhone());
            }
        }

        return responseResult;
    }

    private CustomerResponse convertCustomerResponse(CustomerEntity customerEntity) {
        return CustomerResponse.builder()
                .firstName(customerEntity.getFirstName())
                .lastName(customerEntity.getLastName())
                .address(customerEntity.getAddress())
                .phone(customerEntity.getPhone())
                .type(customerEntity.getType())
                .isDeleted(customerEntity.isDeleted())
                .createdTime(customerEntity.getCreatedTime())
                .createdBy(customerEntity.getCreatedBy())
                .updatedTime(customerEntity.getUpdatedTime())
                .updatedBy(customerEntity.getUpdatedBy()).build();
    }

}
