package com.omg.customercenter.service.impl;

import com.omg.customercenter.entity.OrganizationEntity;
import com.omg.customercenter.exception.CustomerCenterBusinessException;
import com.omg.customercenter.model.request.OrganizationRequest;
import com.omg.customercenter.repository.OrganizationRepository;
import com.omg.customercenter.service.IOrganizationService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

/**
 * @author hieudn
 * CategoryServiceImpl
 */
@Service
@Slf4j
public class OrganizationServiceImpl implements IOrganizationService {

    @Autowired
    private OrganizationRepository organizationRepository;

    /**
     * @param request
     * @throws CustomerCenterBusinessException
     */
    @Override
    @Transactional
    public void createOrganization(OrganizationRequest request) throws CustomerCenterBusinessException {

        // Check exist the organization with the code in the database
        if (this.organizationRepository.countAllByCode(request.getCode()) != 0) {
            throw new CustomerCenterBusinessException("duplicate code: " + request.getCode());
        }

        // Prepare data for create new item
        OrganizationEntity organizationEntity = OrganizationEntity.builder().code(request.getCode())
                .name(request.getName())
                .address(request.getAddress())
                .phone(request.getPhone())
                .description(request.getDescription())
                .isDeleted(false).build();

        this.organizationRepository.save(organizationEntity);
    }

}
