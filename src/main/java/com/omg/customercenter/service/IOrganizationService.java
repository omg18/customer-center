package com.omg.customercenter.service;

import com.omg.customercenter.exception.CustomerCenterBusinessException;
import com.omg.customercenter.model.request.OrganizationRequest;

/**
 * @author HieuDN
 */
public interface IOrganizationService {
    void createOrganization(OrganizationRequest organizationRequest) throws CustomerCenterBusinessException;
}
