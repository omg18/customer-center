package com.omg.customercenter.service;

import com.omg.customercenter.exception.CustomerCenterBusinessException;
import com.omg.customercenter.model.request.CustomerRequest;
import com.omg.customercenter.model.request.CustomerSearchRequest;
import com.omg.customercenter.model.response.CustomerResponse;

/**
 * @author HieuDN
 */
public interface ICustomerService {
    void createCustomer(CustomerRequest customerRequest) throws CustomerCenterBusinessException;

    CustomerResponse getCustomerInfo(CustomerSearchRequest customerSearchRequest) throws CustomerCenterBusinessException;
}
