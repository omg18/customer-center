package com.omg.customercenter.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import java.time.LocalDateTime;

/**
 * @author hieutt
 * CommonEntity
 */
@MappedSuperclass
@Setter
@Getter
@NoArgsConstructor
public class BaseEntity {

    @Column(name = "created_time")
    @CreationTimestamp
    protected LocalDateTime createdTime;

    @Column(name = "created_by")
    protected String createdBy;

    @UpdateTimestamp
    @Column(name = "updated_time")
    protected LocalDateTime updatedTime;

    @Column(name = "updated_by")
    protected String updatedBy;

    @Column(name = "is_deleted")
    protected Integer isDeleted;

}
