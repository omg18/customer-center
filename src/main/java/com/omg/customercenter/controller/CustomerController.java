package com.omg.customercenter.controller;

import com.omg.customercenter.exception.CustomerCenterBusinessException;
import com.omg.customercenter.model.request.CustomerRequest;
import com.omg.customercenter.model.request.CustomerSearchRequest;
import com.omg.customercenter.model.response.CustomerResponse;
import com.omg.customercenter.service.ICustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

/**
 * @author HieuDN
 * Category Controller
 */
@RestController
@RequestMapping("api/customer")
public class CustomerController {

    @Autowired
    private ICustomerService customerService;

    /**
     * Create customer
     *
     * @param request CustomerRequest
     */
    @PostMapping(path = "/create")
    public void create(@RequestBody @Valid CustomerRequest request) throws CustomerCenterBusinessException {
        this.customerService.createCustomer(request);
    }

    @PostMapping("/getCustomerInfo")
    public CustomerResponse getCustomerInfo(@RequestBody @Valid CustomerSearchRequest request) throws CustomerCenterBusinessException {
        return this.customerService.getCustomerInfo(request);
    }
}
