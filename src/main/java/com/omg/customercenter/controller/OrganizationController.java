package com.omg.customercenter.controller;

import com.omg.customercenter.exception.CustomerCenterBusinessException;
import com.omg.customercenter.model.request.OrganizationRequest;
import com.omg.customercenter.service.IOrganizationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

/**
 * @author HieuDN
 * Category Controller
 */
@RestController
@RequestMapping("api/organization")
public class OrganizationController {

    @Autowired
    private IOrganizationService organizationService;

    /**
     * Create customer
     *
     * @param request CustomerRequest
     */
    @PostMapping(path = "/create")
    public void create(@RequestBody @Valid OrganizationRequest request) throws CustomerCenterBusinessException {
        this.organizationService.createOrganization(request);
    }
}
