package com.omg.customercenter.repository;

import com.omg.customercenter.entity.OrganizationEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Optional;

/**
 * @author HieuDN
 */
@Repository
public interface OrganizationRepository extends JpaRepository<OrganizationEntity, Long> {

    int countAllByCode(String code);

    @Query(value = "SELECT * FROM organization o WHERE o.id = :id AND o.is_deleted != 1", nativeQuery = true)
    Optional<OrganizationEntity> findById(@Param("id") Long id);

}
