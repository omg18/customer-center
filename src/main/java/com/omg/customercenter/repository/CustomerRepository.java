package com.omg.customercenter.repository;

import com.omg.customercenter.entity.CustomerEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Optional;

/**
 * @author HieuDN
 */
@Repository
public interface CustomerRepository extends JpaRepository<CustomerEntity, Long> {

    @Query(value = "SELECT * FROM customer c WHERE c.id = :id AND c.is_deleted != 1", nativeQuery = true)
    Optional<CustomerEntity> findById(@Param("id") Long id);
}
